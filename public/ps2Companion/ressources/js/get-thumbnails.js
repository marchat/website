const defaultImageFolder = 'ressources/static/';
const allowedExtensions = ['.jpg', '.jpeg', '.png', '.gif', '.svg'];

function getExtension(filename) {
    return filename.slice(filename.lastIndexOf('.')).toLowerCase();
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function getThumbnails(currentdirectory) {
    fetch(currentdirectory)
        .then(response => response.text())
        .then(html => {
            const root = 'default';
            const parser = new DOMParser();
            const doc = parser.parseFromString(html, 'text/html');
            const elements = doc.querySelectorAll('a');

            const elementsNames = Array.from(elements).map(element => element.innerText);

            const directoryNames = elementsNames
                .map(name => name.split('/')[name.split('/').length - 1])
                .sort()
                .map(fileName => (fileName === '.' ? root : fileName))
                .filter(
                    imageName => !allowedExtensions.includes(getExtension(imageName))
                );

            const filteredImages = elementsNames.filter(imageName =>
                allowedExtensions.includes(getExtension(imageName))
            );

            directoryNames.forEach(directoryName => {
                const imagesHtmlElement = filteredImages
                    .filter(
                        imageDir =>
                            (imageDir.split('/').length > 2 &&
                                imageDir.split('/').includes(`${directoryName}`)) ||
                            (imageDir.split('/').length === 2 && directoryName === root)
                    )
                    .map(imageDir => {
                        const imageName =
                            imageDir.split('/')[imageDir.split('/').length - 1];
                        const thumbnail = document.createElement('div');
                        thumbnail.classList.add('thumbnail');
                        thumbnail.title = `${imageName}`;
                        const anchor = document.createElement('a');
                        anchor.href = `${currentdirectory}${imageDir}`;
                        anchor.innerHTML = `<img src="${currentdirectory}${imageDir}" alt="${imageDir}">`;
                        thumbnail.appendChild(anchor);
                        return thumbnail;
                    });

                const cardDirectory = document.createElement('div');
                cardDirectory.classList = ['directory'];
                const cardTitle = document.createElement('span');
                cardTitle.classList = ['directory-title'];
                cardTitle.innerText = capitalizeFirstLetter(directoryName);
                const cardContent = document.createElement('div');
                cardContent.classList = ['directory-content'];
                cardContent.append(...imagesHtmlElement);
                cardDirectory.append(cardTitle, cardContent);

                if (imagesHtmlElement?.length > 0 && directoryName?.length > 0) {
                    const thumbnailsContainer = document.getElementById('thumbnails');
                    thumbnailsContainer.append(cardDirectory);
                }
            });
        })
        .catch(error => console.error(error));
}

const imageFolder =
    document.currentScript.getAttribute('imageFolder') || defaultImageFolder;
getThumbnails(imageFolder);
